﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MeteoStation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[] array = InputArray();
            GetMaxRainQuantity(array);
            GetMinRainQuantity(array);
            GetAverageRainQuantity(array);
        }

        public static int[] InputArray()
        {
            int[] array = new int[31];
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"Enter data for day {i+1}: ");
                array[i] = int.Parse(Console.ReadLine());
            }

            return array;
        }

        public static void GetMaxRainQuantity(int[] array)
        {
            int max = array[0];
            int maxIndex = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                    maxIndex = i;
                }
            }

            Console.WriteLine($"Max quantity at Day {maxIndex+1}: {max} l/sq.m.");
        }

        public static void GetMinRainQuantity(int[] array)
        {
            int min = array[0];
            int minIndex = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                    minIndex = i;
                }
            }

            Console.WriteLine($"Min quantity at Day {minIndex + 1}: {min} l/sq.m.");
        }

        public static void GetAverageRainQuantity(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            Console.WriteLine($"The average quantity is: {sum / 31.0:F2}");
        }
    }
}